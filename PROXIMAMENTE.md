---
layout: page
title: "PROXIMAMENTE"
permalink: /next/
image: images/ojo.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/ojo.jpg)


**1° TEMPORADA 2021**

1. **EL NO PODER**
2. **LA BUENA MUERTE**
3. **LA VIDA ES UN VODEVIL**
4. **MINDFULNESS, AUTOAYUDA, ZEN, LIDERAZGO**
5. **ANARQUISTAS**
6. **MAGIA**
7. **HABLANDO DE MUJERES Y MISOGINIAS**
8. **CAMBIO CLIMÁTICO**
9. **REALISMO POLITICO**
10. **ACUMULADORES**
11. **VIRTUALIDAD**
12. **LA CENSURA**
13. **ESPIRITUALIDAD**
14. **TODO SOBRE LA MUERTE**
15. **LA IGUALDAD**
16. **INTELIGENCIAS**
17. **LA TRAMPA DE LA DIVERSIDAD**

**2° TEMPORADA 2023**

1. **ALQUIMIA**

