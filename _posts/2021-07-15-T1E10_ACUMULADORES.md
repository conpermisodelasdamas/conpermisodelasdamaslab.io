---
layout: post  
title: "T1E10 ACUMULADORES"  
date: 2021-07-15  
categories: podcast  
image: images/T1E12_ACUMULADORES2.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E10_ACUMULADORES_002.jpg
podcast_link: ia801409.us.archive.org/30/items/t-1-e-12-acumuladores/T1E12_ACUMULADORES.mp3
tags: [ Bill Gates, Steve Jobs]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E12_ACUMULADORES2.gif)

<audio controls>
    <source src="https://ia801409.us.archive.org/30/items/t-1-e-12-acumuladores/T1E12_ACUMULADORES.mp3" type="audio/mpeg">
</audio>

**T1E10 ACUMULADORES**

La posesión de bienes, el acceso privilegiado a servicios, la acumulación de experiencias vitales singulares.

La exclusividad, también es importante en este rubro. Pues ese difícil acceso hace que estos bienes al ser escasos sean muy apreciados.

Los humanos más que egoístas podemos ser avaros.

De entre nosotros existe una casta especial, la de aquellos que enriqueciéndose deciden regalar su fortuna.

Donarla.

Son los filántropos.

¿Esa casta humana realmente existe?

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
