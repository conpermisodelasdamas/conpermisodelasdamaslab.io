---
layout: post  
title: "T1E8 CAMBIO CLIMÁTICO"  
date: 2021-07-01  
categories: podcast  
image: images/T1E8_CAMBIO_CLIMATICO.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E8_CAMBIO_CLIMATICO_002.jpg
podcast_link: ia801503.us.archive.org/13/items/t-1-e-8-cambio-climatico/T1E8_CAMBIO_CLIMATICO.mp3
tags: [greta thunberg, cambio climático, calentamiento global, efecto invernadero, era del hielo]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E8_CAMBIO_CLIMATICO.gif)

<audio controls>
    <source src="https://ia801503.us.archive.org/13/items/t-1-e-8-cambio-climatico/T1E8_CAMBIO_CLIMATICO.mp3" type="audio/mpeg">
</audio>

**T1E8 CAMBIO CLIMÁTICO**

Tendemos a decir que el planeta tierra es "nuestro planeta", es decir que nos pertenece. 

Y esa noción hace emanar de nuestras mentes la idea de que se nos está acabando, se nos termina el planeta, porque como recurso que es, lo estamos mal administrando. 

Greta dice que las anteriores generaciones se han farreado "su planeta" y que ella se está quedando sin nada, es decir sin futuro.

Otra perspectiva posible es mirar esto desde esa máxima juridica que dice: "lo accesorio corre la suerte de lo principal".

¿Qué es lo principal?

¿Quién es lo accesorio?

El planeta es un ser vivo, que como tal, se enferma, convalesce y sana.

Quizás, los humanos para el planeta, somos simples ácaros que medramos en su epidermis. 

Una sarna a la que se debe matar.

Muerto el perro, muerta la rabia.

Quizás el planeta esté en eso, viendo como quitarse esta molesta sarna de encima.

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
