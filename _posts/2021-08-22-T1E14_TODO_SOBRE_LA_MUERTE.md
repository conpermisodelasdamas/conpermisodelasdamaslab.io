---
layout: post  
title: "T1E14 TODO SOBRE LA MUERTE"  
date: 2021-08-12  
categories: podcast  
image: images/T1E15_TODO_SOBRE_LA_MUERTE.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E14_TODO_SOBRE_LA_MUERTE_002.jpg
podcast_link: ia801506.us.archive.org/15/items/t-1-e-15-todo-sobre-la-muerte/T1E15_TODO_SOBRE_LA_MUERTE.mp3
tags: [muerte, inmortalidad, Maturana, funeraria, ataud]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E15_TODO_SOBRE_LA_MUERTE.gif)

<audio controls>
    <source src="https://ia801506.us.archive.org/15/items/t-1-e-15-todo-sobre-la-muerte/T1E15_TODO_SOBRE_LA_MUERTE.mp3" type="audio/mpeg">
</audio>

**T1E14 TODO SOBRE LA MUERTE**

Ser un profesional, es ser dueño de una actitud particular frente al problema propuesto, una actitud que hoy por hoy se dice "agrega valor".

Cuando todo se ha perdido, cuando ni la vida ya queda, cuanto el suceso de la muerte aconteció, queda muy poco valor que agregar, y pareciera ser que frente a la muerte no es posible tener una actitud proactiva.

Peor el caso cuando quién se acerca a la hora fatal, sabe que está acercando su nariz a la cornisa de la nada y no sabe qué hacer frente a algo tan, pero tan vacío. 

Sócrates recomendó centrarse en la vida, la buena vida, de nuestra propia buena vida, agrego yo, ya los vivos se encargarán de las muertes que a otros sobrevengan.

Tal como necesitamos insumos para vivir, los requerimos para morir, para bien morir, una creencia, una posición filosófica y en caso de dolor: analgésicos.

Los que quedan con vida requieren procesar a sus muertos, primero deshacerse de sus despojos y luego olvidarlos los más mansamente que sea posible.

La muerte como suceso, es el más cierto. Y a la vez el más desconocido.

En este episodio nos adentramos de la mano de un profesional en todo sobre la muerte.

mientras estemos con vida...

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
