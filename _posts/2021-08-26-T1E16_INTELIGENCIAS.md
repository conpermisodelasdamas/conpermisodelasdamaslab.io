---
layout: post  
title: "T1E16 INTELIGENCIAS"  
date: 2021-08-26  
categories: podcast  
image: images/T1E16_INTELIGENCIAS.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E16_INTELIGENCIAS_002.jpg 
podcast_link: ia601500.us.archive.org/16/items/t-1-e-16-inteligencias/T1E16_INTELIGENCIAS.mp3
tags: [daniel golemann, inteligencias, Howard Gardner]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E16_INTELIGENCIAS.gif)

<audio controls>
    <source src="https://ia601500.us.archive.org/16/items/t-1-e-16-inteligencias/T1E16_INTELIGENCIAS.mp3" type="audio/mpeg">
</audio>

**INTELIGENCIAS**

El ciego no ve y suple este sentido desarrollando los demás a su haber con otras habilidades que compensen en conjunto a la vista.  

¿Se podrá afirmar que ese ciego es dueño de otras inteligencias, por tener estas habilidades desarrolladas?

¿Acaso es el cerebro el único órgano propietario de la inteligencia?

¿Los instintos serán maneras de inteligencia? 

Y dentro de ellos, el instinto sexual: ¿A qué tipo de inteligencia se pertenecería?

Un tonto de capirote, hábil para patear la pelota, que gana cientos de millones de euros al año: ¿Es dueño de una inteligencia superior?

**NUEVAMENTE** Un polémico asunto finamente tratado, mediante eternas contradicciones que en este caso, si podrían conducir a algo.

Todo depende quién escucha.

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  

Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.