---
layout: post  
title: "T1E13 ESPIRITUALIDAD"  
date: 2021-08-05  
categories: podcast  
image: images/T1E14_ESPIRITUALIDAD_CARATULA.gif 
image2: https://conpermisodelasdamas.gitlab.io//images/T1E13_ESPIRITUALIDAD_002.jpg
podcast_link: ia601502.us.archive.org/21/items/t-1-e-14-espiritualidad/T1E14_ESPIRITUALIDAD.mp3
tags: [buda, krishna, Maimónides, Tibet]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E14_ESPIRITUALIDAD_CARATULA.gif)

<audio controls>
    <source src="https://ia601502.us.archive.org/21/items/t-1-e-14-espiritualidad/T1E14_ESPIRITUALIDAD.mp3" type="audio/mpeg">
</audio>

**ESPIRITUALIDAD**

La fe, ese gran misterio que nos avoca al dogma.

Lo siento, la fe no tiene nada que ver con la espiritualidad.

Al menos con ese tipo de ejercicio espiritual que te libera, que hace a tu ser más amplio, que expande las fronteras de tu consciencia.

La fe es una actitud de aceptación voluntaria a una cárcel vivencial. Siempre hay una recompensa a cambio de semejante flagelo. 

Promesa que no consta como de título ejecutivo. Es decir, en los hechos no parece vinculante.

La fe y la religión se llevan bien, y por medio de determinados artefactos litúrgicos nos crean la fantasía de la experiencia espiritual.

Pero, bien lo dijo Carlitos en el XIX: esa es una droga. Pues te envilece y por lo mismo surte el efecto contrario al del ejercicio espiritual.

La espiritualidad es un camino lóbrego, lleno de dudas y plagado de tinieblas.

Es difícil y sólo apto para trabajadores, valientes y sacrificados espíritus, que así evitan ser martirizados en pos de alguna siempre ajena causa.


Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  

Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
