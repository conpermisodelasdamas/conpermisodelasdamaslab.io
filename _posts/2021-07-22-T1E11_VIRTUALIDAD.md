---
layout: post  
title: "T1E11 VIRTUALIDAD"  
date: 2021-07-22  
categories: podcast  
image: images/T1E10_VIRTUALIDAD.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E11_VIRTUALIDAD_002.jpg
podcast_link: ia801507.us.archive.org/1/items/t-1-e-10-virtualidad/T1E10_VIRTUALIDAD.mp3
tags: [ajedrez, verso improvisado, Arnold Schwarzenegger]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E10_VIRTUALIDAD.gif)

<audio controls>
    <source src="https://ia801507.us.archive.org/1/items/t-1-e-10-virtualidad/T1E10_VIRTUALIDAD.mp3" type="audio/mpeg">
</audio>

**T1E11 VIRTUALIDAD**

En el audio digo que Arnold Schwarzenegger es un gran actor, ahora al escribir meditando cada letra, no estoy tan seguro de lo que mi boca habla.

Lo que si es cierto es que en la peli **Total Recall** escrita hace más 30 años, su protagonista Douglas Quaid interpretado por Arnold, convence que le borraron la mente para crearle una realidad virtual.

Otra peli, Matrix, también del siglo XX, engendro de hollywood nos habla del mismo fenómeno.

Llegado cierto punto, todo se podría hacer virtual.

¿Y que es esto de la virtualidad?

¿Por qué nos negamos a ella a pesar de que la deseamos con ansias?

Seguimos aquí y ahora . . .

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
