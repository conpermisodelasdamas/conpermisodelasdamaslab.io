---
layout: post  
title: "T1E15 LA IGUALDAD"  
date: 2021-08-19  
categories: podcast  
image: images/T1E13_LA_IGUALDAD.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E15_LA_IGUALDAD_002.jpg
podcast_link: ia801506.us.archive.org/19/items/t-1-e-13-igualdad/T1E13_IGUALDAD.mp3
tags: [IGUALDAD]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E13_LA_IGUALDAD.gif)

<audio controls>
    <source src="https://ia801506.us.archive.org/19/items/t-1-e-13-igualdad/T1E13_IGUALDAD.mp3" type="audio/mpeg">
</audio>

**T1E15 LA IGUALDAD**

La verdad es que el tema de este episodio debiera ser la des-igualdad, esa lacerante realidad que nos rodea y frente a la cual es imposible redimirnos, todos somos culpables, sin excepción.

La foto que ilustra este post es la de Aylan Kurdi, un niño de tres años ahogado en una playa turca mientras escapaba junto a su familia de Siria, es decir de la guerra en Siria. Huía del Daesh, para ser exactos.

Eso pasó el 2 de septiembre de 2015. 

**OTRO NOMBRE:** George Floyd, el hombre negro muerto por la rodilla de un policía el 26 de mayo de 2020.

**OTRO LUGAR:** Texas, algún punto del Rio Bravo frente al estado de Guerrero, una niña de "chamarra" y tenis rojos llora por su madre, junio de 2018.

**OTRA ÉPOCA:**  Madrid 5 de agosto de 1939, Carmen Barrero Aguado, Martina Barroso García, Blanca Brisac Vázquez, Pilar Bueno Ibáñez, Julia Conesa Conesa, Adelina García Casillas, Elena Gil Olaya, Virtudes González García, Ana López Gallego, Joaquina López Laffite, Dionisia Manzanero Salas, Victoria Muñoz García y Luisa Rodríguez de la Fuente, las **TRECE ROSAS**.

Todas historias horripilantes que nos muestran los bordes afilados de la des-igualdad.

Este episodio deja intuir esta y otras cosas.

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
