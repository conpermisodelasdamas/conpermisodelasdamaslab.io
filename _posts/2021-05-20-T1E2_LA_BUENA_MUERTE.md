---
layout: post  
title: "T1E2 La Buena Muerte."  
date: 2021-05-20  
categories: podcast  
image: images/T1E2_LA_BUENA_MUERTE.png
image2: https://conpermisodelasdamas.gitlab.io//images/T1E2_LA_BUENA_MUERTE_002.jpg
podcast_link: ia601405.us.archive.org/17/items/t-1-e-2-la-buena-muerte/T1E2_LA_BUENA_MUERTE.mp3
tags: [muerte, aborto, eutanasia]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E2_LA_BUENA_MUERTE.png)

<audio controls>
    <source src="https://ia601405.us.archive.org/17/items/t-1-e-2-la-buena-muerte/T1E2_LA_BUENA_MUERTE.mp3" type="audio/mpeg">
</audio>

**t1e2 La Buena Muerte.**

¿Reflexiones filosóficas?

Tratar temas bioéticos como el aborto y la eutanasia, implica imperativos morales y con gran carga valórica como la libertad, la responsabilidad, el respeto a la vida, la religión, la espiritualidad, la ciencia, el bien común, el pragmatismo y la salud pública. Hasta dónde son válidas las fronteras entre mi cuerpo, mi decisión, y un Estado y una superestructura que regule, norme y restrinja nuestras posibilidades para decidir sobre nuestro propio ser, plenamente conscientes de lo que hacemos o todo lo contrario, en estados de desesperación, enajenación o lucidez. ¿Hasta dónde es válido alentar y reivindicar derechos subjetivos (individuales y autonómicos), y a la vez rehuir y renegar de nuestras responsabilidades y compromisos con el otro?

Este es un debate que nunca va a terminar y que siempre generará más interrogantes y dudas que respuestas y conclusiones definitivas. En este sentido, una conversación habitual e informal entre dos amigos, puede devenir en un ejercicio de inteligencia y de diálogo, con un espíritu de camaradería, responsabilidad y de búsqueda de ideas que articulen lógicas, éticas y puntos de vista, con los cuales podemos concordar o diferir dentro del plano de las ideas, lejos de cualquier carga emotiva y de rechazo que nos pudiera generar el tratamiento de estos temas.

Se debe considerar también el significado existencial de la muerte, de nuestra propia muerte, y el hecho de asumir su certeza e inevitabilidad, viviendo día a día, frente al hecho absoluto de que nadie ha salido vivo de esta aventura que emprendimos al nacer.

Si decides involucrarte en la escucha atenta de esta conversación, estas divagaciones subjetivas te permitirán elaborar tus propias reflexiones filosóficas al respecto para enriquezcas, provoques y estimules el debate y te enfrentes a tu libertad y a una vida y una muerte (una buena muerte), ya sea con sentido o sinsentido. 

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
