---
layout: post  
title: "T1E5 ANARQUISTAS"  
date: 2021-06-10  
categories: podcast  
image: images/MINIATURA.png
image2: https://conpermisodelasdamas.gitlab.io//images/T1E5_ANARQUISTAS_002.jpg
podcast_link: ia601407.us.archive.org/30/items/t-1-e-4-anarquismo-3-er-proceso/T1E4_ANARQUISMO_3ER_PROCESO.mp3
tags: [sacco y vanzetti, Bakunin, Proudhon, Asange, anonymous]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/MINIATURA.png)

<audio controls>
    <source src="https://ia601407.us.archive.org/30/items/t-1-e-4-anarquismo-3-er-proceso/T1E4_ANARQUISMO_3ER_PROCESO.mp3" type="audio/mpeg">
</audio>

**T1E5 ANARQUISTAS**

El sueño de libertad, entendida esta en su sentido mas amplio, más extenso e infinito, tiene nombre propio: ANARAQUÍA. 

Al mismo tiempo es poseedora de la peor prensa que se puede concebir. Sacco y Vanzetti (su foto adorna esta entrada), fueron chivos expiatorios de los temores infoundidos por el poder ante una sociedad que estaba en proceso de creación. 

Y, ese adjetivo: ANARQUISTA, les jugó en contra, pagaron con su vida la justificación de los temores de una sociedad ante un concepto desconocido. Una moderna "caza de brujas".

No nos quedó más que destripar al anarquismo. 

Y aunque suene raro: ambos (cuestionado y cuestionador), desde nuestra propia experiencia de vida, más o menos reflexiva, somos **ANARQUISTAS**.   


Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
