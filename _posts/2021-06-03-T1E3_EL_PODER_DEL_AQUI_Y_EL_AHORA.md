---
layout: post  
title: "T1E4 MINDFULNESS, AUTOAYUDA, ZEN, LIDERAZGO"  
date: 2021-06-03  
categories: podcast  
image: images/T1E3_EL_PODER_DEL_AQUI_Y_EL_AHORA.png
image2: https://conpermisodelasdamas.gitlab.io//images/T1E4_MINDFULNESS_AUTOAYUDA_ZEN_LIDERAZGO_002.jpg
podcast_link: ia601502.us.archive.org/2/items/t-1-e-3-el-poder-del-aqui-y-el-ahora/T1E3_EL_PODER_DEL_AQUI_Y_EL_AHORA.mp3
tags: [mindfulness, autoayuda, zen, liderazgo]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E3_EL_PODER_DEL_AQUI_Y_EL_AHORA.png)

<audio controls>
    <source src="https://ia601502.us.archive.org/2/items/t-1-e-3-el-poder-del-aqui-y-el-ahora/T1E3_EL_PODER_DEL_AQUI_Y_EL_AHORA.mp3" type="audio/mpeg">
</audio>

**t1e4 MINDFULNESS, AUTOAYUDA, ZEN, LIDERAZGO**

Si nos has escuchado, a esta altura del partido ya bien supondrás que no vamos a hablar tan libremente de temas de autoayuda, ni de mindfulness. Cosas que para nosotros, almas liberales y escépticas, nos parecen esotéricas por lo incomprensible de sus propuestas tan “prácticas” y fáciles de ejecutar. Nuestra naturaleza desconfía ante magras propuestas que sólo requieren de una porción de voluntad, una miga de esperanza y un insignificante grano de fe. Para nosotros, esto de la fe, no es materia que abunde mucho. No te dejes engañar por este provocador titular, no es lo que parece.

Claro que no mentimos, la propuesta es una conversación respecto del poder, un poder que se radica en un asunto que pareciera ser producto del azar. El aquí y el ahora, no son aspectos tan simples de ver, ni de considerar. ¿Cómo llegamos a este lugar en este preciso momento? Esa es LA PREGUNTA, que nos agobia de vez en cuando. Hacemos aquí otro de nuestros amagos filosóficos destripando el tema de varias maneras posibles. Y, como siempre, concluiremos que no existen los absolutos, que la totalidad de la existencia humana no se puede explicar a través de un recetario que cure todos los males.


Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
