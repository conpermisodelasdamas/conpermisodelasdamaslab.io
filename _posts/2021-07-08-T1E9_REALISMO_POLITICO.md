---
layout: post  
title: "T1E9 REALISMO POLITICO"  
date: 2021-07-08  
categories: podcast  
image: images/T1E9_REALISMO_CARATULA.gif
image2: https://conpermisodelasdamas.gitlab.io//images/T1E9_REALISMO_POLITICO_002.jpg 
podcast_link: ia601506.us.archive.org/10/items/t-1-e-9-realismo-politico/T1E9_REALISMO_POLITICO.mp3
tags: [realismo politico, maquiavelo, hobbes]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E9_REALISMO_CARATULA.gif)

<audio controls>
    <source src="https://ia601506.us.archive.org/10/items/t-1-e-9-realismo-politico/T1E9_REALISMO_POLITICO.mp3" type="audio/mpeg">
</audio>

**T1E9 REALISMO POLITICO**

Tomás Moro creó su Utopía, eso fue en el siglo 16. 

Le costó la cabeza. 

No fué realista. A pesar de que su definición del "deber ser" en la cosa pública es encomiable.

Pero, las cosas se manejan de otra manera. Más al estilo Corleone.

Sobre todo, si en el plano de las relaciones internacionales hablamos de realismo político . 

Importantes avances ha tenido la humanidad restringiendo a los absolutismos por modos mas civilizados, es algo evidente.

El fondo sigue ahí mismo: los poderes fácticos.

Y, al igual que esas misteriosas asociaciones para delinquir de Sicilia, mantienen su rostro oculto.

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  

Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
