---
layout: post  
title: "T1E1 El NO poder."  
date: 2021-05-13  
categories: podcast  
image: images/IGNACIO_REYES.jpg
image2: https://conpermisodelasdamas.gitlab.io//images/T1E1_EL_NO_PODER_002.jpg  
podcast_link: ia601508.us.archive.org/9/items/piloto-el-no-poder/PILOTO_EL_NO_PODER.mp3
tags: [audio, poesia, octosilabo]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/IGNACIO_REYES.jpg)

<audio controls>
    <source src="https://ia601508.us.archive.org/9/items/piloto-el-no-poder/PILOTO_EL_NO_PODER.mp3" type="audio/mpeg">
</audio>

**t1e1 El NO poder.**

Poco ha pasado desde la elección presidencial que con seguridad, diremos, cambiará todo en Ecuador, para años después caer en cuenta que la verdad, verdad no cambio nada. ¡Ni una puta cosa! El respeto a las formas, y los gestos de condescendencia pronto mutarán en la salvaje lucha por asegurar el suficiente poder que permita a los sátrapas de siempre mantener sus satrapías o perderlas a favor de otros reyezuelos. Aquellos llamados a velar por los destinos de la patria, harán su esfuerzo para ir contra los elementos. Para al fin, conformarse con uno que otro deslucido logro logístico: mucha gente vacunada, muchos contratos firmados a precios no tan dudosos e indicadores macroeconómicos con buena tendencia. Pero, dirán, no se pudo más: no estaban las condiciones, y un largo etcétera. Conversamos muy a salto de mata, de la tremenda decepción que siempre los políticos nos deparan, y peor aún, aquellos que fungen de no serlo. Finalmente... ¡Qué chucha!  


Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
