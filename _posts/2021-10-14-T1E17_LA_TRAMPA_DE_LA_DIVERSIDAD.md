---
layout: post  
title: "T1E17 LA TRAMPA DE LA DIVERSIDAD"  
date: 2021-10-14  
categories: podcast  
image: images/ T2E1_LA_TRAMPA_DE_LA_DIVERSIDAD_CARATULA.gif
image2: https://conpermisodelasdamas.gitlab.io//images/E1E17_LA_TRAMPA_DE_LA_DIVERSIDAD_002.jpg 
podcast_link: ia601403.us.archive.org/2/items/t-2-e-1-la-trampa-de-la-diversidad/T2E1_LA_TRAMPA_DE_LA_DIVERSIDAD.mp3
tags: [Napoleón Saltos, movimientos sociales, convención constituyente chile]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T2E1_LA_TRAMPA_DE_LA_DIVERSIDAD_CARATULA.gif)

<audio controls>
    <source src="https://ia601403.us.archive.org/2/items/t-2-e-1-la-trampa-de-la-diversidad/T2E1_LA_TRAMPA_DE_LA_DIVERSIDAD.mp3" type="audio/mpeg">
</audio>

**LA TRAMPA DE LA DIVERSIDAD**

Napoleón Saltos es el sociólogo por antonomasia de Ecuador, además ofició como político pero le fue mal por ser honesto. Extrañamente pretendía que sus Honorables colegas Diputados ejercieran limpiamente sus mandatos. Muy loco este Napoleón.

Lo tuvimos para hablar de eso, de política, de luchas sociales, de la manera en que esas se entrampan, se enrollan y se complican, justamente por ser la expresión de los intereses trasparentes de las clases desposeídas.

Una excusa perfecta para analizar estos asuntos es la experiencia chilena de Asamblea Constituyente (Convención Constitucional le dicen por allá).

Un organismo mandatado para escribir un nuevo pacto social para Chile, compuesto por una multiplicidad de personas diferentes, diversas, muchas veces contrarias y contradictorias.

Un espejo para mirar a las sociedades ene que nos encontramos en este continente, multiétnicas, multilingües, multi TODO.

Y con las generaciones X marcando la agenda como nunca antes pasó, gentes muy informadas, bien o mal pero con mucha cantidad de datos, y con algunas cosas claras, quieren cambiarlo todo AHORA.

je je je 

Finalmente: solo tenemos lo que podemos... Ya! ¡Qué chucha!  

Cualquier cosita nos escribes a:


+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.